<?php

namespace Drupal\assembly\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\TypedData\TranslatableDataInterface;

class AssemblyViewBuilder extends EntityViewBuilder {
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    $service = \Drupal::service('plugin.manager.assembly_build');
    $plugins = $service->getDefinitions();
    $build['#parent'] = $this->getContext($entity);

    foreach ($plugins as $plugin) {
      if (in_array($entity->bundle(), $plugin['types'])) {
        $service->createInstance($plugin['id'])->build($build, $entity, $display, $view_mode);
      }
    }
  }

  private function getContext($entity) {
    if (!isset($entity->_referringItem)) {
      return FALSE;
    }

    // Get the entity reference
    $ref = $entity->_referringItem;

    // Get info about the field
    $field = $ref->getParent();
    $field_name = $field->getName();

    // Get the entity referencing the assembly
    $parent = $ref->getEntity();
    $parent_type = $parent->getEntityTypeId();

    return ['entity' => $parent, 'entity_type' => $parent_type, 'field_name' => $field_name];
  }

  /**
   * Provides entity-specific defaults to the build process.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which the defaults should be provided.
   * @param string $view_mode
   *   The view mode that should be used.
   *
   * @return array
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    // Allow modules to change the view mode.
    $context = [];
    $this->moduleHandler()->alter('entity_view_mode', $view_mode, $entity, $context);

    $build = [
      "#{$this->entityTypeId}" => $entity,
      '#view_mode' => $view_mode,
      // Collect cache defaults for this entity.
      '#cache' => [
        'tags' => Cache::mergeTags($this->getCacheTags(), $entity->getCacheTags()),
        'contexts' => $entity->getCacheContexts(),
        'max-age' => $entity->getCacheMaxAge(),
      ],
      '#contextual_links' => [
        'assembly' => [
          'route_parameters' => ['assembly' => $entity->id()]
        ]
      ],
    ];

    // Add the default #theme key if a template exists for it.
    if ($this->themeRegistry->getRuntime()->has($this->entityTypeId)) {
      $build['#theme'] = $this->entityTypeId;
    }

    // Cache the rendered output if permitted by the view mode and global entity
    // type configuration.
    if ($this->isViewModeCacheable($view_mode) && !$entity->isNew() && $entity->isDefaultRevision() && $this->entityType->isRenderCacheable()) {
      $build['#cache'] += [
        'keys' => [
          'entity_view',
          $this->entityTypeId,
          $entity->id(),
          $view_mode,
        ],
        'bin' => $this->cacheBin,
      ];

      if ($entity instanceof TranslatableDataInterface && count($entity->getTranslationLanguages()) > 1) {
        $build['#cache']['keys'][] = $entity->language()->getId();
      }
    }

    return $build;
  }
}
