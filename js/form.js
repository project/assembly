(function ($, Drupal, drupalSettings) {

Drupal.behaviors.assemblyForm = {
  attach: function(context, settings) {
    $(once('assemblyForm', '[data-assembly-revision-checkbox]')).each(function() {
      var $checkbox = $(this), $log = $checkbox.closest('.ief-form').find('[data-assembly-revision-log]');
      // Only do this if there's a log message to show/hide
      if (!$log.length) {
        return;
      }
      // Create a toggle link
      var $toggle = $('<a class="assembly-toggle-revision-log" href="#">Add a log message</a>').on('click', function(e) {
        e.preventDefault();
        $toggle.text($log.is(':visible') ? 'Add a log message' : 'Clear log message' );
        $log.find('textarea').val('');
        $log.toggle();
      });

      // add it after the relevant label and add a helper class. Maybe i should use [for=""] here
      $checkbox
        .closest('.form-type-checkbox')
        .addClass('assembly-revision-checkbox')
        .find('label')
          .first()
          .after($toggle)
      ;

      // Don't show the toggle if revision is unchecked
      $checkbox.on('change', function() {
        $toggle.toggle($checkbox.is(':checked'));
      });

      // Init the stuff
      $log.toggle();
      $toggle.toggle($checkbox.is(':checked'));

    });
    $(once('assemblyReferences', '[data-assembly-references-id]')).each(function() {
      var $this = $(this);
      $.get('/assembly/' + $(this).data('assembly-references-id') + '/references', function(res) {
        res = res.filter(function(i) { return i !== drupalSettings.path.currentPath});
        if (res.length < 1) {
          return;
        }
        var $ul = $this.append('<strong>Used In: </strong> <ul class="inline list-inline"></div>').find('ul');
        for (var index = 0; index < Math.min(5, res.length); index++) {
          var o = res[index];
          $ul.append('<li><a href="' + o.url + '" target="_blank">' + o.label + '</li>');
        }

        if (res.length > 5) {
          $ul.append('<li>... and ' + (res.length-5) + ' more</li>');
        }
      });
    });
  }
}

})(jQuery, Drupal, drupalSettings);
